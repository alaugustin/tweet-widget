// Avoid `console` errors in browsers that lack a console.
( function() {
		var method;
		var noop = function noop() {
		};
		var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
		var length = methods.length;
		var console = (window.console = window.console || {});

		while (length--) {
			method = methods[length];

			// Only stub undefined methods.
			if (!console[method]) {
				console[method] = noop;
			}
		}
	}());

// Place any jQuery/helper plugins in here.

function turnYqlToJson() {
	/**
	 * Parse JSON with jQuery
	 * Creates an unordered list out of JSON objects
	 */
	$.ajax({
		type : "GET",
		url : "http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20feed%20WHERE%20url%3D'http%3A%2F%2Fsearch.twitter.com%2Fsearch.rss%3Fq%3D%2523sandy'&format=json&diagnostics=true&callback=tweetWidget",
		data : {
			get_param : 'value'
		},
		jsonpCallback : 'tweetWidget',
		dataType : "jsonp",
		success : function(data) {
			var obj = data.query.results.item, // get entry object (array) from JSON data
			section = $('<section id="tweetWidget">');
			// create a new section element
			// iterate over the array and build the list
			for (var i = 0, l = obj.length; i < l; ++i) {
				section.append('<article class="tweetEntry"><img class="profileImg" src="' + obj[i].content.url + '" height="' + obj[i].content.height + '" width="' + obj[i].content.width + '" ><a href="' + obj[i].link + '" target="_blank">' + obj[i].title + '</a><div>' + obj[i].pubDate + '</div></article>');
			}
			$("#results").append(section);
			// add the list to the DOM
		}
	});
}

